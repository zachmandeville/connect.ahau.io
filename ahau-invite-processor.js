const express = require('express')
const cors = require('cors')
const app = express()

app.use(cors())
// TODO only allow connections from connect.ahau.io

app.get('/', function (req, res, next) {
  const invite = req.query.invite
  if (!invite) return res.json({ msg: 'No invite code' })

  console.log({ invite })

  // process ssb invite
  // ssb.invite.use(invite, cb) ...
  setTimeout(() => {
    Math.random() < 0.5 // simulate invites working/ failing
      ? res.json({ msg: 'success' })
      : res.json({ msg: 'failure' })
  }, 1000)
})

app.listen(12345, () => {
  console.log('listening for invites')
})
